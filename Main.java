package com.greatlearning.assignment;

import java.util.Scanner;

public class Main
{
   public static void main(String[] args) 
   {
     while (true)
     {
        Scanner abc = new Scanner(System.in);
        //choosing the required department
        System.out.println(  "Enter 1 for Admin department. "+
                             "Enter 2 for HR department. "+
                             "Enter 3 for Tech department. "   +
                             "Enter 4 for Exit. ");
        int m=abc.nextInt();
        switch(m)
        {
        
          //displaying functionalities of Admin department
          case 1 :
          AdminDepartment pri1 = new AdminDepartment();
          System.out.println(pri1.depName());
          System.out.println(pri1.getTodaysWork());
          System.out.println(pri1.getWorkDeadline());
          System.out.println(pri1.isTodayAHoliday());
          break;
          //Displaying functionalities of HR department
          case 2 :
          HrDepartment pri2 = new HrDepartment();
          System.out.println(pri2.depName());
          System.out.println(pri2.getTodaysWork());
          System.out.println(pri2.doactivity());
          System.out.println(pri2.getWorkDeadline());
          System.out.println(pri2.isTodayAHoliday());
          break;
          //displaying functionalities of Technical department
          case 3 :
          TechnicalDepartment pri3 = new TechnicalDepartment();
          System.out.println(pri3.depName());
          System.out.println(pri3.getTodaysWork());
          System.out.println(pri3.getTechStackInformation());
          System.out.println(pri3.getWorkDeadline());
          System.out.println(pri3.isTodayAHoliday());
          break;
          //continuing to exit
          case 4:
          System.exit(1);
         //if entered incorrect option then default will execute
          default:
          System.out.println("Enter invalid option");
         }
    }
 }
}
