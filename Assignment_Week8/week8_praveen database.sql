create database week8_praveen;
use week8_praveen;
create table bookusers(
username varchar(30) primary key,
password varchar(30)
);

create table books(
id int primary key,
name varchar(50),
author varchar(50),
url varchar(500)
);

insert into books values(1,"Piranesi","Susanna Clarke (2021)","https://media.wired.co.uk/photos/617816d6b7a9cc58f009bb8a/master/w_1600,c_limit/26082021_B_02.jpg");
insert into books values(2,"Stardust","Neil Gaiman (2010)","https://media.wired.co.uk/photos/617816cee82d86967f88fe5f/master/w_1600,c_limit/26082021_B_04.jpg");
insert into books values(3,"Jade City"," Fonda Lee (2018)","https://media.wired.co.uk/photos/617816cce82d86967f88fe5d/master/w_640,c_limit/26082021_B_01.jpg");
insert into books values(4,"The Last Unicorn","Peter S Beagle (1968)","https://media.wired.co.uk/photos/617816ce987920b1978506ec/master/w_640,c_limit/26082021_B_05.jpg");

insert into books values(5,"A Court of Thorns and Roses","Sarah J. Maas (2020)","https://media.wired.co.uk/photos/606d9f230286a2e569b12c39/master/w_1600,c_limit/wired-books-5.jpg");
insert into books values(6,"The Power","Naomi Alderman (2017)","https://media.wired.co.uk/photos/606d9f23938ecee6e930e6c8/master/w_1600,c_limit/wired-books-29.jpg");
insert into books values(7,"The Fifth Season","N. K. Jemisin (2016)","https://media.wired.co.uk/photos/606d9f236ab54fce4fbb1ebc/master/w_960,c_limit/wired-books-26.jpg");


create table likelist(
id int primary key,
name varchar(50),
author varchar(50),
url varchar(500)
);
create table readlater(
id int primary key,
name varchar(50),
author varchar(50),
url varchar(500)
);