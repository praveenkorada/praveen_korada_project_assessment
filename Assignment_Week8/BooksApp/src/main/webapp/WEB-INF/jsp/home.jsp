<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home Page</title>
<style type="text/css">
body{
      background-color:#ADFF2F;
    }
 h1 {
  font-family: cursive;
  font-size: 50px;
  letter-spacing: -2px;
}
.container1{
  position: relative;
  right: 10;
  margin: 20px;
  max-width: 300px;
  padding: 16px;
  background-color: #00BFFF;
}
.container {
  position: relative;
  right: 0;
  margin: 20px;
  max-width: 300px;
  padding: 16px;
  background-color: #00BFFF;
}
 #header {
        background-color: #B0E0E6;
        color: #fff;
    }

</style>
</head>
<body>
<h1 style="text-align: center;">WELCOME TO BOOKESS BOOK STORE</h1>
   <form action="login" class="container1">
    <label for="login"><b>Login Here</b></label>
    <input type="submit" value="Click Here To Login">
    </form><br>
    <form action="register" class="container">
    <label for="register"><b>Register Here</b></label>
    <input type="submit" value="Click Here To Register">
    </form>
    

</body>
</html>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<%@ page import="java.util.*"%>
	<%@ page import="com.greatlearning.week8.bean.*"%>
	<h3>BOOKS STORE APPLICATION</h3><hr>

<table align="center" cellpadding="5" cellspacing="5" border="2">
	<thead>
          <tr bgcolor="#DEB887">
             <th>Book Id</th>
             <th>Name</th>
             <th>Author</th>
             <th>Image</th>
          </tr>
      </thead>
      
      <tbody>
      	<c:forEach var="i" items="${books}">
			<tr align = "center" bgcolor="#DC143C">
				<td><c:out value="${i.getId() }"></c:out></td>
				<td><c:out value="${i.getName() }"></c:out></td>
				<td><c:out value="${i.getAuthor() }"></c:out></td>
				<td><img src=<c:out value="${i.getUrl() }"></c:out>width="100" height="150"></td>
				
			</tr>
		</c:forEach> 
      </tbody>
      </table>

