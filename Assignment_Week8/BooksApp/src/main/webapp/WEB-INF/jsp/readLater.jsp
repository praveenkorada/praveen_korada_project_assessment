<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Read Later</title>
<style>
body {
        padding: 0px;
        margin: 0;
        font-family: Verdana, Geneva, Tahoma, sans-serif;
        background-color: #98FB98
    }
    
    
    
    tr {
        transition: all .2s ease-in;
        cursor: pointer;
    }
    
    th,
    td {
        padding: 12px;
        text-align: left;
        border-bottom: 1px solid #ddd;
    }
    
    #header {
        background-color: #16a085;
        color: #fff;
    }
    
    h1 {
  font-family: cursive;
  font-size: 50px;
  letter-spacing: -2px;
}
.btn {
              background-color: rgb(94, 169, 204);
              border-color: blue;
              color: white;
              padding: 12px 16px;
              font-size: 18px;
              cursor: pointer;
              border-radius: 12px;
              width: 150px;
              margin: 15px;

            }
            
            /* Darker background on mouse-over */
            .btn:hover {
              background-color: rgb(5, 44, 160);
            }
    tr:hover {
        background-color: #f5f5f5;
        transform: scale(1.02);
        box-shadow: 2px 2px 12px rgba(0, 0, 0, 0.2), -1px -1px 8px rgba(0, 0, 0, 0.2);
    }
    
    @media only screen and (max-width: 768px) {
        table {
            width: 90%;
        }
    }
</style>
</head>
<body>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<h1> READ LATER BOOKS</h1><hr>
	<center>
	<%
	String username = (String) session.getAttribute("username");
	if (username == null)
		username = "";
	out.print("<h3>It's me!!!  " + username + " </h3><br/ >");
	%>
			<hr>
			</center>
			<input type='button' value='Logout'
			onclick="location.href= 'login'" class="btn">
	
	<table align="center" cellpadding="5" cellspacing="5" border="2">
		<tr bgcolor="#DEB887">
			<th><b>BookId</b></th>
			<th><b>Name</b></th>
			<th><b>Author</b></th>
			<th><b>Image</b></th>		
		</tr>
		<c:forEach var="i" items="${books}">
			<tr align = "center" bgcolor="#DC143C">
					<td><c:out value="${i.getId() }"></c:out></td>
					<td><c:out value="${i.getName() }"></c:out></td>
					<td><c:out value="${i.getAuthor() }"></c:out></td>
					<td><img src=<c:out value="${i.getUrl() }"></c:out>width="100" height="175"></td>
   					<td><a href="readLaterDelete/${i.getId()}">Remove</a></td>  			
			</tr>
		</c:forEach>
	</table>
	<br/><br/>
	
</body>
</html>