<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body, html {
  height: 100%;
  font-family: Arial, Helvetica, sans-serif;
}

* {
  box-sizing: border-box;
}
.bg-img {
  /* The image used */
  background-image: url("https://cdn.pixabay.com/photo/2017/08/07/19/07/books-2606859__340.jpg");

  min-height: 600px;

  /* Center and scale the image nicely */
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;
}

/* Add styles to the form container */
.container {
  position: absolute;
  right: 0;
  margin: 20px;
  max-width: 300px;
  padding: 16px;
  background-color: #98fb98;
}

/* Full-width input fields */
input[type=text], input[type=password] {
  width: 100%;
  padding: 15px;
  margin: 5px 0 22px 0;
  border: none;
  background: #f1f1f1;
}

input[type=text]:focus, input[type=password]:focus {
  background-color: #ddd;
  outline: none;
}

/* Set a style for the submit button */
.btn {
  background-color: #04AA6D;
  color: white;
  padding: 16px 20px;
  border: none;
  cursor: pointer;
  width: 100%;
  opacity: 0.9;
}

.btn:hover {
  opacity: 1;
}
h1 {
  font-family: cursive;
  font-size: 50px;
  letter-spacing: -2px;
}
</style>
</head>
<body>
<center>
<h1>Bookess Book-Store</h1>
</center>
<h2>Please Register Here</h2>
<div class="bg-img">
  <form action="register" method="post" class="container">
		Username: <input type="text" name="username" /> <br> <br>
		Password: <input type="password" name="password" /><br> <br>
		<input type="submit" value="Register" class="btn">
		Already have Account!! <a href="login">Login</a>
	</form>
</div>

</body>
</html>
