<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>After Login</title>
<style>
 body {
        padding: 0px;
        margin: 0;
        font-family: Verdana, Geneva, Tahoma, sans-serif;
        background-color: #EE82EE;
    }
     h1 {
  font-family: cursive;
  font-size: 50px;
  letter-spacing: -2px;
}
    
    
    tr {
        transition: all .2s ease-in;
        cursor: pointer;
    }
    
    th,
    td {
        padding: 22px;
        text-align: left;
        border-bottom: 1px solid #ddd;
    }
    
    #header {
        background-color: #16a085;
        color: #fff;
    }
    .btn {
              background-color: rgb(94, 169, 204);
              border-color: blue;
              color: white;
              padding: 12px 16px;
              font-size: 18px;
              cursor: pointer;
              border-radius: 12px;
              width: 150px;
              margin: 15px;

            }
            
            /* Darker background on mouse-over */
            .btn:hover {
              background-color: rgb(5, 44, 160);
            }
    .btn1 {
              background-color: #32cd32);
              border-color: #2f4f4f;
              color: #800000;
              padding: 10px 12px;
              font-size: 14px;
              cursor: pointer;
              border-radius: 12px;
              width: 110px;
              margin: 10px;

            }
            
            /* Darker background on mouse-over */
            .btn1:hover {
              background-color: #008000);
            }
    
    tr:hover {
        background-color: #f5f5f5;
        transform: scale(1.02);
        box-shadow: 2px 2px 12px rgba(0, 0, 0, 0.2), -1px -1px 8px rgba(0, 0, 0, 0.2);
    }
    #32cd32
    @media only screen and (max-width: 768px) {
        table {
            width: 60%;
        }
    }
</style>
</head>
<body>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

	<h1>BOOKESS BOOK-STORE</h1><hr>
	<%
	String username = request.getParameter("username");
	out.print("<center><h2>HELLO!!! Mr. " + username + " </h2></center<br/ >");
	session.setAttribute("username", username);
	%>
	<input type='button' value='Logout'
			onclick="location.href= 'login'" class="btn">
			<hr>
	<h2>Available Books in Store</h2>
	<br />
	<br />
	<table align="center" cellpadding="5" cellspacing="5" border="1" >

		<tr bgcolor="#DEB887">
			<th><b>BookId</b></th>
			<th><b>Name</b></th>
			<th><b>Author</b></th>
			<th><b>Image</b></th>
			<th><b>Add To</b></th>
			<th><b>Add To</b></th>
		</tr>


		<c:forEach var="i" items="${books}">
		
			<tr align = "center"  bgcolor="#DC143C">
				<td>
					<form method="post">
						<c:out value="${i.getId() }"></c:out>
						<input name="id" value="${i.getId()}" hidden="true">
				</td>
				<td><c:out value="${i.getName() }"></c:out><input name="name"
					value="${i.getName()}" hidden="true"></td>
					
				<td><c:out value="${i.getAuthor() }"></c:out><input
					name="author" value="${i.getAuthor()}" hidden="true"></td>
					
				<td><img src=<c:out value="${i.getUrl() }"></c:out>width="100" height="175"><input
					name="url" value="${i.getUrl() }" hidden ="true"></td>
					
				<td><input type='submit' value='Like list'
					onclick="this.form.action='liked';" class="btn1"></td>
						
				<td ><input type='submit' value='ReadLater list'
					onclick="this.form.action='readLater';" class="btn1">
					</form></td>
			</tr>
		</c:forEach>
	</table>
	<br/><br/>
</body>
</html>