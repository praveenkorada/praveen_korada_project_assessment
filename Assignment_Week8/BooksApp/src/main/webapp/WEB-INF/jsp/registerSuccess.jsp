<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<style>
body {
        padding: 0px;
        margin: 0;
        font-family: Verdana, Geneva, Tahoma, sans-serif;
        background-color: #FFFFCC;
    }
    
    
    #header {
        background-color: #16a085;
        color: #fff;
    }
    
    h1 {
  font-family: cursive;
  font-size: 50px;
  letter-spacing: -2px;
}
    
     @media only screen and (max-width: 768px) {
        table {
            width: 90%;
        }
    }
</style>
</head>
<body>
<h1>BOOKESS BOOK-STORE</h1>
<br/><br/><center>
<h2>Hello!! Mr. <strong> ${username} </strong> You have been registered successfully!!
<br/> <br/>
Click here to login</h2><hr>  
<a href ="login">Login </a>
</center>
</body>
</html>