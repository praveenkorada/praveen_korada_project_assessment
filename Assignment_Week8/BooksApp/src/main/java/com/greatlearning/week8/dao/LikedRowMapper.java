package com.greatlearning.week8.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.greatlearning.week8.bean.*;

public class LikedRowMapper implements RowMapper {

	@Override
	public LikedBooks mapRow(ResultSet rs, int rowNum) throws SQLException {
		LikedBooks books = new LikedBooks();
		books.setId(rs.getInt("id"));
		books.setName(rs.getString("name"));
		books.setAuthor(rs.getString("author"));
		books.setUrl(rs.getString("url"));
		return books;
		
	}

}
