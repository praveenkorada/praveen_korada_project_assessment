package com.greatlearning.week8.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.greatlearning.week8.bean.*;

public class ReadLaterRowMapper implements RowMapper {

	@Override
	public ReadLaterBooks mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		ReadLaterBooks books = new ReadLaterBooks();
		books.setId(rs.getInt("id"));
		books.setName(rs.getString("name"));
		books.setAuthor(rs.getString("author"));
		books.setUrl(rs.getString("url"));
		return books;
	}

}
