package com.greatlearning.week8.controller;

import java.util.List;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.greatlearning.week8.bean.*;
import com.greatlearning.week8.service.*;

@Controller
public class BookController {
	
	@Autowired
	LoginService logserve;
	
	@RequestMapping("/login")  
	public String login()
	{
		return "login";
	}
	
	@RequestMapping("/register")  
	public String register()
	{
		return "register";
	}
	
	@Autowired
	BooksService bookServe;
	
	@RequestMapping("/home")  
    public ModelAndView home()  
    {  
		ModelAndView mv = new ModelAndView();
		mv.setViewName("home");
		mv.addObject("books",bookServe.getAllBooks());
		System.out.println("success");
		System.out.println(bookServe.getAllBooks());
        return mv;  
    } 
	
	@Autowired
	RegisterService service;
	
	@PostMapping("/register")
	public ModelAndView registerUser(@RequestParam("username") String username, @RequestParam("password") String password) {

		try {
			service.registerUser(username, password);
			ModelAndView mv = new ModelAndView();
			mv.setViewName("registerSuccess");
			mv.addObject("username",username);
			return mv;
		} catch (DuplicateKeyException e) {
			ModelAndView mv = new ModelAndView();
			mv.setViewName("registerError");
			return mv;
		}
	}
	
	@Autowired
	LikedBooksService likeserve;
	
	@PostMapping("/liked")  
    public ModelAndView Likedlist(@RequestParam("id") int id,@RequestParam("name") String name,
			@RequestParam("author") String author,@RequestParam("url") String url )  
    {  
		try
		{
			likeserve.addBooks(id, name, author, url);
			ModelAndView mv = new ModelAndView();
			mv.setViewName("liked");
			System.out.println("Added to Liked List");
			mv.addObject("books",likeserve.getAllBooks());
			System.out.println(likeserve.getAllBooks());
			return mv; 
		}
		catch(DuplicateKeyException e)
		{
			ModelAndView mv = new ModelAndView();
			mv.setViewName("likedError");
			return mv;
		}  
    }
	
	@Autowired
	ReadLaterService readserve;
	
	@PostMapping("/readLater")  
    public ModelAndView readLater(@RequestParam("id") int id,@RequestParam("name") String name,
			@RequestParam("author") String author, @RequestParam("url") String url)  
    {  
		try
		{
			readserve.addBooks(id, name, author, url);
			ModelAndView mv = new ModelAndView();
			mv.setViewName("readLater");
			System.out.println("Added to Read Later List");
			mv.addObject("books",readserve.getAllBooks());
			System.out.println(readserve.getAllBooks());
			return mv; 
		}
		catch(DuplicateKeyException e)
		{
			ModelAndView mv = new ModelAndView();
			mv.setViewName("readLaterError");
			return mv;
		}    
    }
	
	@GetMapping("/readLater")  
    public ModelAndView readLaterAfterDelete()  
    {  
		
			ModelAndView mv = new ModelAndView();
			mv.setViewName("readLater");
			mv.addObject("books",readserve.getAllBooks());
			System.out.println(readserve.getAllBooks());
			return mv;          
    }
	@GetMapping("/liked")  
    public ModelAndView LikedAfterDelete()  
    {  
		
			ModelAndView mv = new ModelAndView();
			mv.setViewName("liked");
			mv.addObject("books",likeserve.getAllBooks());
			System.out.println(likeserve.getAllBooks());
			return mv;      
    }
	
	@PostMapping("/login")
	public ModelAndView validateUser(@RequestParam("username") String username,@RequestParam("password") String password) {

		if (logserve.validateUser(username, password))
		{
			ModelAndView mv = new ModelAndView();
			mv.setViewName("successlogin");
			mv.addObject("books",bookServe.getAllBooks());
			System.out.println("Success");
			System.out.println(bookServe.getAllBooks());
	        return mv; 
		}
		else {
			ModelAndView mv = new ModelAndView();
			mv.setViewName("loginError");
			return mv;
		}
	}
		
	@GetMapping("/readLaterDelete/{id}")
	public String readLaterDelete(@PathVariable("id") int id) {
		readserve.deleteBook(id);
		return "redirect:/readLater";
	}
	
	@GetMapping("/likedDelete/{id}")
	public String likedDelete(@PathVariable("id") int id) {
		likeserve.deleteBook(id);
		return "redirect:/liked";
	}	
}
