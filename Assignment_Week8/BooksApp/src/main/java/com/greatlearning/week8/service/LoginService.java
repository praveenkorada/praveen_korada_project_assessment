package com.greatlearning.week8.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.greatlearning.week8.dao.*;

public class LoginService {

	@Autowired
	LoginDAO dao ;

	public boolean validateUser(String username, String password) {
		return dao.validateUser(username, password);
	}
}
