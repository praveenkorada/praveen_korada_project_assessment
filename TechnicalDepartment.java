package com.greatlearning.assignment;

public class TechnicalDepartment extends SuperDepartment
{
	    public String depName() 
	    {
	       return "TECH DEPARTMENT";
	    }
		public String getTodaysWork()
		{
		   return "COMPLETE CODING OF MODULE 1";
		}
		public String getTechStackInformation()
		{
		   return "CORE JAVA";
		}
		public String getWorkDeadline() 
		{
		   return "COMPLETE IT BY END OF DAY";
		}
		
		
}
