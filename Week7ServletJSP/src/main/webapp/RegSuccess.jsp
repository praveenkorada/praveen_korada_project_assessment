<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
	 .btn {
              background-color: rgb(94, 169, 204);
              border-color: blue;
              color: white;
              padding: 12px 16px;
              font-size: 18px;
              cursor: pointer;
              border-radius: 12px;
              width: 150px;
              margin: 15px;

            }
            
            /* Darker background on mouse-over */
            .btn:hover {
              background-color: rgb(5, 44, 160);
            }
</style>
</head>
<body>
<h1>Register successfully</h1>
<form action="login.html">
<input type="submit" value="Login Here" class="btn">
</form>
</body>
</html>