package com.bean;

public class Book {

	private int BookId;
	private String name;
	private String type;
	private String author;
	private float price;
	
	public Book(int bookId, String name, String type, String author, float price) {
		super();
		this.BookId = bookId;
		this.name = name;
		this.type = type;
		this.author = author;
		this.price = price;
	}
	public Book() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getBookId() {
		return BookId;
	}
	public void setBookId(int bookId) {
		BookId = bookId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
//	  <servlet>
//	   <servlet-name>BookController</servlet-name>
//	   <servlet-class>BookController</servlet-class>
//	</servlet>
//	 
//	<servlet-mapping>
//	   <servlet-name>BookController</servlet-name>
//	   <url-pattern>/BookController</url-pattern>
}
	