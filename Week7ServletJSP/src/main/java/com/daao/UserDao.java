package com.daao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.bean.User;

public class UserDao {
	private Connection con;	
	private PreparedStatement ps;
	public UserDao() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.cj.jdbc.Driver");
		con = DriverManager.getConnection("jdbc:mysql://localhost:3306/week9_praveen","root","5A4m267p");
	}
	
	public void userRegister(User user) throws SQLException {
		int count;
		ps = con.prepareStatement("insert into bookusers values(?,?)");
		ps.setString(1, user.getUsername());
		ps.setString(2, user.getPassword());
		count = ps.executeUpdate();
		ps.close();
	}
	

	public Boolean getUser(String username, String password) throws SQLException {
		ps = con.prepareStatement("select *from bookusers");
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
		
			if(rs.getString(1).equals(username) && rs.getString(2).equals(password)) {
				ps.close();
				return true;	
			}
		}
		ps.close();
		return false;
	}
	public void readLater(String username, String password, String bookid) throws SQLException {
		ps = con.prepareStatement("insert into ReadLater(username,password,booksid) values(?,?,?)");
		PreparedStatement ps2 = con.prepareStatement("select *from ReadLater");
		ResultSet rs = ps2.executeQuery();
		while(rs.next()) {
		
			if(rs.getString(1).equals(username) && rs.getString(2).equals(password) && rs.getString(3).equals(bookid)) {
				return;	
			}
		}
		ps.setString(1, username);
		ps.setString(2, password);
		ps.setString(3, bookid);
		ps.executeUpdate();
		ps.close();
		ps2.close();
		rs.close();
	}
	public void Likelist(String username, String password, String bookid) throws SQLException {
		ps = con.prepareStatement("insert into likelist(username,password,bookid) values(?,?,?)");
		PreparedStatement ps2 = con.prepareStatement("select *from likelist");
		ResultSet rs = ps2.executeQuery();
		while(rs.next()) {
		
			if(rs.getString(1).equals(username) && rs.getString(2).equals(password) && rs.getString(3).equals(bookid)) {
				return;	
			}
		}
		ps.setString(1, username);
		ps.setString(2, password);
		ps.setString(3, bookid);
		ps.executeUpdate();	
		ps.close();
		ps2.close();
		rs.close();	
	}
	

}
