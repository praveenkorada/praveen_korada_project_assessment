package com.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bean.User;
import com.daao.UserDao;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static User currentUser;
	private UserDao userDao;
	
	public void init() {
		try {
			userDao = new UserDao();
		} catch ( ClassNotFoundException e) {
			e.printStackTrace();
		}catch (SQLException e) {
			e.printStackTrace();
		}
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username;
		String password;
		username = request.getParameter("luname");
		password = request.getParameter("lpwd");
		try {
			if(userDao.getUser(username, password)) {
				currentUser = new User();
				currentUser.setUsername(username);
				currentUser.setPassword(password);
				response.sendRedirect("UserData.jsp");
			}else {
				response.sendRedirect("LoginFail.jsp");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
