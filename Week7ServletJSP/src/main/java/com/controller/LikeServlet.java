package com.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.controller.LoginServlet;
import com.daao.UserDao;
import com.bean.*;


public class LikeServlet extends HttpServlet {

	private UserDao userDao;
    
    public void init() {
    	try {
			userDao = new UserDao();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}	
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String bookid = request.getParameter("bookid");
		User user = LoginServlet.currentUser;
		String username = user.getUsername();
		String password = user.getPassword();
		
		try {
			userDao.Likelist(username, password, bookid);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		response.setContentType("text/html");
		PrintWriter pw = response.getWriter();
		pw.println("<html><body><h1>Book added successfully to liked section</h1></body></html>");
	}
}
