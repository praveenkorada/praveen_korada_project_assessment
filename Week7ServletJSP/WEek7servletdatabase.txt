create database bookstore;
use bookstore;
create table Users(
username varchar(30) primary key,
password varchar(30)
);
desc users;
select *from users;


create table books(
bookid int primary key,
name varchar(30),
type varchar(30),
author varchar(30),
price float
);
desc books;



insert into books values(111, "alfreds futterkiste", "inspiring" ,"alfred schmid",543);
insert into books values(112, "Berglunds snabbkop",  "action","Berguvsvagen8",444);
insert into books values(113, "he-man",  "action", "bauk",876);
insert into books values(114, "nick",  "auto","ola",678);
insert into books values(115, "doreamon", "robotic","sirji",999);
insert into books values(116, "nobita", "bestt","quot",547);
insert into books values(117, "shuzuka", "romantic","jiya",234);
insert into books values(118, "jeon",  "archer","matlab",333);
insert into books values(119, "sin-chan", "comedy","hapo",654);
insert into books values(110, "wings of day",  "cartoon","mkoi",987);
insert into books values(121, " wings of night",  "zones","mkoi",587);
insert into books values(122, "wings of fightt",  " tip sultan","mkoi",487);
insert into books values(123, "now you see me",  "tip sultan","mkoi",287);
insert into books values(124, "now you see me2",  "sultan","mkoi",187);
insert into books values(125, "nobody",  "cartoon","suleman",787);
insert into books values(126, "nobody345 ",  "novel","nananji",687);
insert into books values(127, "see yourself",  "poetry ","romeo",587);
insert into books values(128, "you and me",  "romantic","malya",487);
insert into books values(129, "you and me",  "romantic","malya",387);
insert into books values(130, "jnnody",  "sapien","malya",287);

select *from books;


create table likelist(
bookid int primary key,
name varchar(30),
type varchar(30),
author varchar(30),
price float
);
desc likelist;
select *from likelist;

create table readlater(
bookid int primary key,
name varchar(30),
type varchar(30),
author varchar(30),
price float
);
desc readlater;
select *from readlater;


