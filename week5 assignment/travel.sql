
create database travel_praveen;

use travel_praveen;

create table PASSENGER
 (Passenger_name varchar(20), 
  Category               varchar(20),
   Gender                 varchar(20),
   Boarding_City      varchar(20),
   Destination_City   varchar(20),
  Distance                int,
  Bus_Type             varchar(20)
);


create table PRICE
(
             Bus_Type   varchar(20),
             Distance    int,
              Price      int
          );

insert into passenger values('Sejal','AC','F','Bengaluru','Chennai',350,'Sleeper');
insert into passenger values('Anmol','Non-AC','M','Mumbai','Hyderabad',700,'Sitting');
insert into passenger values('Pallavi','AC','F','panaji','Bengaluru',600,'Sleeper');
insert into passenger values('Khusboo','AC','F','Chennai','Mumbai',1500,'Sleeper');
insert into passenger values('Udit','Non-AC','M','Trivandrum','panaji',1000,'Sleeper');
insert into passenger values('Ankur','AC','M','Nagpur','Hyderabad',500,'Sitting');
insert into passenger values('Hemant','Non-AC','M','panaji','Mumbai',700,'Sleeper');
insert into passenger values('Manish','Non-AC','M','Hyderabad','Bengaluru',500,'Sitting');
insert into passenger values('Piyush','AC','M','Pune','Nagpur',700,'Sitting');

select * from passenger;

insert into price values('Sleeper',350,770);
insert into price values('Sleeper',500,1100);
insert into price values('Sleeper',600,1320);
insert into price values('Sleeper',700,1540);
insert into price values('Sleeper',1000,2200);
insert into price values('Sleeper',1200,2640);
insert into price values('Sleeper',350,434);
insert into price values('Sitting',500,620);
insert into price values('Sitting',500,620);
insert into price values('Sitting',600,744);
insert into price values('Sitting',700,868);
insert into price values('Sitting',1000,1240);
insert into price values('Sitting',1200,1488);
insert into price values('Sitting',1500,1860);

select * from price;

select count(case when UPPER(gender)='M'then 1 end)Male,count(case when UPPER(gender)='F'then 1 end)Female from passenger where distance>=600 ; --1

select min(price) as Min_price_for_sleeper_bus from price;--2

select *from passenger where passenger_name like 's%';--3

select p.passenger_name as 'passenger',p.boarding_city as 'Boarding',p.destination_city as 'Destination',p.bus_type as 'type of bus',pp.price as 'ticket_fare' from passenger p,price pp;--4

select p.passenger_name,p.bus_type,pp.price as'ticket fare'from passenger p,price pp where p.bus_type="Sitting" AND p.distance=1000;--5

 select distinct p.passenger_name,p.boarding_city as Destination_city,p.Destination_city as Boarding_city, p.Bus_Type, pp.Price from passenger p,price pp where Passenger_name="Pallavi" and p.Distance=pp.Distance;--6

select distinct distance from passenger order by distance desc;--7

select passenger_name,distance*100.0/(select sum(distance)from passenger)from passenger group by distance;--8

create view p_view as select *from passenger where Category='AC';select * from p_view;--9

--10 i am unable to do sir

select * from passenger limit 5;--11








