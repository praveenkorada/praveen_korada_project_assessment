package com.greatlearning.Week3Assignment;
import java.util.Scanner;
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		MagicOfBooks mb = new MagicOfBooks();
		while (true) {
			System.out.println("Choose one option:\n"+ "1.To Add Books\r\n"
		                       + "2.To delete entries from books\r\n" + "3.To Update Book \r\n"
					           + "4.To dispaly all books \r\n" + "5.Total count of Books\r\n"
		                       +"6.Search Autobiography Books\n" + "7.To Display by features");
			System.out.println("Enter Your Choice:");
			int choice = scanner.nextInt();
			switch(choice) {
				case 1://Adding a book
					   System.out.println("ENter No.of Books to Add:");
					   int n= scanner.nextInt();
					   for(int i=1;i<=n;i++) {
						   mb.AddBook();
						   
					   }
					   break;
				case 2: //Delete a Book
					   try {
						   mb.DeleteBook();
					   }catch(CustomException e) {
						   System.out.println(e.getMessage());
					   }break;
				case 3: //Update a book
					try {
						   mb.UpdateBook();
					   }catch(CustomException e) {
						   System.out.println(e.getMessage());
					   }break;
				case 4: // Display Books
					 try {
						   mb.DisplayBookInfo();
					   }catch(CustomException e) {
						   System.out.println(e.getMessage());
					   }break;
				case 5: //Count Books
					try {
						   mb.count();
					   }catch(CustomException e) {
						   System.out.println(e.getMessage());
					   }break;
				case 6: //autobiography Search
					try {
						   mb.AutoBiography();
					   }catch(CustomException e) {
						   System.out.println(e.getMessage());
					   }break;
				case 7:// Search by feature
					    System.out.println("Enter Your choice : \n 1.Price Low to High"
					    		           + "\n 2.Price High to Low\n 3.Best Selling");
					    int ch = scanner.nextInt();
					    switch (ch)
					  {
					    case 1: try {
					    	   mb.DisplayByFeature(1);
					    } catch (CustomException e) {
					    	System.out.println(e.getMessage());
					    }break;
				        case 2:  try {
					    	   mb.DisplayByFeature(2);
					    } catch (CustomException e) {
					    	System.out.println(e.getMessage());	    
					    }break;
				        case 3: try {
					    	   mb.DisplayByFeature(3);
					    } catch (CustomException e) {
					    	System.out.println(e.getMessage());
					
			            }break;
		                             
		              }
					    default: System.out.println("You've entered wrong choice.!");

	              }
           }
	}
}
	
