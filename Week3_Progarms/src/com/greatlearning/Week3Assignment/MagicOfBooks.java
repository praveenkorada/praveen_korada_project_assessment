package com.greatlearning.Week3Assignment;
import java.util.*;
public class MagicOfBooks<T> {
	Scanner scanner = new Scanner(System.in);
	HashMap<Integer,BookDetails> BookMap = new HashMap<>();
	TreeMap<Double,BookDetails> treemap= new TreeMap<>();
	
	ArrayList<BookDetails> booklist= new ArrayList<>();
	//Adding Books
	public void AddBook() {
		BookDetails b= new BookDetails();
		System.out.println("Enter Book ID:");
		b.setID(scanner.nextInt());
		
		System.out.println("Enter Book Name ; ");
		b.setName(scanner.next());
		
		System.out.println("Enter Book Price:");
		b.setPrice(scanner.nextDouble());
		
		System.out.println("Enter Book Genre(AutoBiography-AutoBiography) :");
		b.setGenre(scanner.next());
		
		System.out.println("Enter Number Of COpies Sold :");
		b.setNoOfCOpiesSold(scanner.nextInt());
		
		System.out.println("Enter Book Status(B-bestselling):");
		b.setBookStatus(scanner.next());
		
		BookMap.put(b.getID(),b);
		System.out.println("Book Added Successfully");
		
		treemap.put(b.getPrice(),b);
		booklist.add(b);
		
	}
	//Deleting Books
	public void DeleteBook()throws CustomException {
		if(BookMap.isEmpty()) {
			throw new CustomException("No Books are avaliable to delete!!");
		}
		else {
			System.out.println("Enter Book ID to delete:");
			int ID = scanner.nextInt();
			BookMap.remove(ID);
			System.out.println("Successfully deleted!!");
		}
	}
	//Updating Books
	public void UpdateBook()throws CustomException {
		if(BookMap.isEmpty()) {
			throw new CustomException("No Books are avaliable to Update");
		}
		else {
			BookDetails b = new BookDetails();
			System.out.println("Enter Book ID :");
			b.setID(scanner.nextInt());
			
			System.out.println("Enter Book name:");
			b.setName(scanner.next());
			
			System.out.println("Enter Book price:");
			b.setPrice(scanner.nextDouble());
			
			System.out.println("Enter Book Genre:");
			b.setGenre(scanner.next());
			
			System.out.println("Enter Number Of copies sold:");
			b.setNoOfCOpiesSold(scanner.nextInt());
			
			System.out.println("Enter Book Status:");
			b.setBookStatus(scanner.next());
			
			BookMap.replace(b.getID(), b);
			System.out.println("Book details Updated Successfully!!");
			
			
		}
	}
	//Displaying Books 
	public void DisplayBookInfo() throws CustomException {
		
		if (BookMap.size()>0)
		{
			Set<Integer> keyset = BookMap.keySet();
			for(Integer key : keyset) {
				System.out.println(key + " ---->" + BookMap.get(key));
				
			}	
		}else  
			throw new CustomException("BookMap is Empty!!");
		
	}
	//Counting 
	public void count() throws CustomException {
		if(BookMap.isEmpty()) 
			throw new CustomException("book store is empty!!");
		else
			System.out.println("Number of Books present in the store :"+BookMap.size());
	}
	//Autobiography search 
	public void AutoBiography() throws CustomException {
		String BestSelling = "AutoBiography";
		if(booklist.isEmpty())
			throw new CustomException("Book store is Empty!!");
		else {
			ArrayList<BookDetails> genreBookList = new ArrayList<BookDetails>();
			Iterator<BookDetails> iter = (booklist.iterator());
			while(iter.hasNext()) {
				BookDetails b1 = (BookDetails)iter.next();
				if(b1.Genre.equals(BestSelling))
				{
					genreBookList.add(b1);
					System.out.println(genreBookList);
				}
			}
		}
	}
	//Displaying by feature
	public void DisplayByFeature(int flag) throws CustomException{
		if(flag==1)  //Low to High
		{
			if(BookMap.size()>0)
			{
				Set<Double>keySet = treemap.keySet();
				for(Double key : keySet) {
					System.out.println(key + "---->" + treemap.get(key));
				}
			}else 
				throw new CustomException("book store is empty is Empty!!");
		}
		if(flag==2)//High to Low
		{
			Map<Double,Object> treeMapReverseOrder = new TreeMap<>(treemap);
			//putting values in navigable map
			NavigableMap<Double,Object> Nmap = ((TreeMap<Double,Object>) treeMapReverseOrder).descendingMap();
			System.out.println("Book Details :");
			if(Nmap.size()>0)
			{
				Set<Double>keySet = Nmap.keySet();
				for(Double key : keySet)
					System.out.println(key + "---->" + Nmap.get(key));
			}
			else 
				throw new CustomException("Book Store is Empty!!");
		}
		if(flag==3)//Bestselling
		{
			String BestSelling = "B";
			ArrayList<BookDetails> genreBookList = new ArrayList<BookDetails>();
			Iterator<BookDetails> iter = booklist.iterator();
			while(iter.hasNext())
			{
				BookDetails b = (BookDetails)iter.next();
				if(b.BookStatus.equals(BestSelling))
					genreBookList.add(b);
			}
			if(genreBookList.isEmpty())
				throw new CustomException("No best selling Books are avaliable!!");
			else
				System.out.println("Best Selling Books: "+genreBookList);
		}
	}
	
	

}
