package com.greatlearning.Week3Assignment;

public class BookDetails
{ 
	int ID, NoOfCopiesSold;
	String Name, Genre, BookStatus;
	double price;
	
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		this.ID = iD;
	}
	public int getNoOfCOpiesSold() {
		return NoOfCopiesSold;
	}
	public void setNoOfCOpiesSold(int noOfCopiesSold) {
		this.NoOfCopiesSold = noOfCopiesSold;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		this.Name = name;
	}
	public String getGenre() {
		return Genre;
	}
	public void setGenre(String genre) {
		this.Genre = genre;
	}
	public String getBookStatus() {
		return BookStatus;
	}
	public void setBookStatus(String bookStatus) {
		this.BookStatus = bookStatus;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Book [ID =" + ID + ",Name=" + Name + ", price="+ price + ", Genre=" +Genre + ",NoOfcopiesSold="
				   + NoOfCopiesSold + ",BookStatus=" + BookStatus + "]";
	}

}
