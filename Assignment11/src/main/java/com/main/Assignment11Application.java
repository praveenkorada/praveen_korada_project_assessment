package com.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Assignment11Application {

	public static void main(String[] args) {
		SpringApplication.run(Assignment11Application.class, args);
		System.err.println("Server is running on 8080");
	}

}
