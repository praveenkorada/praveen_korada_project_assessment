package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.UserDao;
import com.entity.User;
import com.exception.ProjectException;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserDao userDao;
	
	@Override
	public User addUser(User user) {
		// TODO Auto-generated method stub
		return userDao.save(user);
	}

	@Override
	public List<User> getAllUsers() {
		// TODO Auto-generated method stub
		return userDao.findAll();
	}

	@Override
	public User getUserById(Integer userId) throws ProjectException {
		// TODO Auto-generated method stub
		return userDao.findById(userId).orElseThrow(() -> new ProjectException("User Id not found"));
	}

	@Override
	public User updateUser(User user) throws ProjectException {
		// TODO Auto-generated method stub
		User u = userDao.findById(user.getId()).orElseThrow(() -> new ProjectException("User Id not found"));
		return userDao.saveAndFlush(u);
	}

	@Override
	public String deleteUserById(Integer userId) throws ProjectException {
		// TODO Auto-generated method stub
		User user = userDao.findById(userId).orElseThrow(() -> new ProjectException("User Id not found"));
		userDao.delete(user);
		return "Deleted Successfully";
	}
}
