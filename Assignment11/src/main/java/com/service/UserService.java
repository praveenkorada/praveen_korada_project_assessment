package com.service;

import java.util.List;

import com.entity.User;
import com.exception.ProjectException;

public interface UserService {

	public User addUser(User user);

	public List<User> getAllUsers();

	public User getUserById(Integer userId) throws ProjectException;

	public User updateUser(User user) throws ProjectException;

	public String deleteUserById(Integer userId) throws ProjectException;

}
