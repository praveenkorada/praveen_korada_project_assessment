package com.service;

import java.util.List;


import com.entity.Books;
import com.exception.ProjectException;

public interface BookService {

	public Books addBook(Books book);

	public List<Books> getAllBooks();

	public Books getBookById(Integer bookId) throws ProjectException;

	public Books updateBook(Books book) throws ProjectException;

	public String deleteBookById(Integer bookId) throws ProjectException;

	public String addBooksToLike(Integer userId, Integer bookId) throws ProjectException;
	
	public List<Books> getLikedBooksByUserId(Integer userId) throws ProjectException;
	
	String deleteBooksFromLike(Integer userId, Integer bookId) throws ProjectException;

}

