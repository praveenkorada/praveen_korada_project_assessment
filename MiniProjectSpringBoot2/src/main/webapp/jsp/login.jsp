<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>

<html>

<head>

<meta charset="ISO-8859-1">
<title>Login Here</title>

</head>

<body>

	<div align=center>
	
		<%
		String message = (String) request.getAttribute("message");
		session.setAttribute("message", message);
		%>
		
		<%if(message!=null){ %>
		<h1 style="font-size: 15px"><%=message%> </h1>
		<% }%>

		<h1>Login Account</h1>

		<form:form id="loginForm" modelAttribute="user" action="login"
			method="post">

			<table>
				<tr>
					<td><form:label path="name">Username: </form:label></td>
					<td><form:input path="name" name="name" id="name" />
					</td>
				</tr>
				<tr>
					<td><form:label path="password">Password:</form:label></td>
					<td><form:password path="password" name="password"
							id="password" /></td>
				</tr>
				<tr>
					<td></td>
					<td align="left"><form:button id="login" name="login">Login</form:button>
					</td>
				</tr>
			</table>
		</form:form>

		<p>
			<a href="register">Create New Account</a>
		</p>
		<p>
			<a href="index">Go To Index</a>
		</p>
	</div>
</body>
</html>









































<%-- <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>

<html>
<head>
<title>Admin Login</title>
<link href="webjars/bootstrap/4.6.0/css/bootstrap.min.css" rel="stylesheet">
<style>
	.admin-login-form{
		font-family:cursive;
		width:400px;
		height:200px;
		top:20%;
		left:50%;
		margin-right:-50%;
		transform:translate(-50%,-50%);
		position:absolute;
		background-color: yellow;
		border-radius: 5%;
	}
		.login-form{
		font-family:cursive;
		width:400px;
		height:200px;
		top:70%;
		left:50%;
		margin-right:-50%;
		transform:translate(-50%,-50%);
		position:absolute;
		background-color: yellow;
		border-radius: 5%;
		
	}
</style>
</head>
<body>
	<h2>${errormsg}</h2>
	<div class="admin-login-form">
		<div class="container-fluid">
			<h4>Admin Login Form</h4>
			<form method="post">
				<input type="text" name="userid" placeholder="USER ID" class="form-control mt-3"/> 
				<input type="password" name="password" placeholder="Password" class="form-control mt-3"/>	
				<input type="submit" name="submit" class="btn btn-dark btn-block mt-3">
		</form>
		</div>
	</div>
	
	
	<h2>${errormsg}</h2>
	<div class="login-form">
		<div class="container-fluid">
			<h4>User Login Form</h4>
			<form method="post">
				<input type="text" name="userid" placeholder="USER ID" class="form-control mt-3"/> 
				<input type="password" name="password" placeholder="Password" class="form-control mt-3"/>	
				<input type="submit" name="submit" class="btn btn-dark btn-block mt-3">
				<input type="submit" value="Do Registeration" class="btn btn-dark btn-block mt-3">
		</form>
		</div>
	</div>
</body>
</html>










 --%>