package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bean.Admin;

@Repository
public interface AdminLogin extends JpaRepository<Admin, Integer> {

	Admin findByUsernameAndPassword(String username, String password);

}
