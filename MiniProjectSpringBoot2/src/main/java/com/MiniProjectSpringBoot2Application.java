package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class }) 
@ComponentScan("com")
@EnableJpaRepositories(basePackages = "com.dao")
@EntityScan
@EnableAutoConfiguration
public class MiniProjectSpringBoot2Application {

	public static void main(String[] args) {
		SpringApplication.run(MiniProjectSpringBoot2Application.class, args);
		System.err.println("running on 8080");
	}

}
