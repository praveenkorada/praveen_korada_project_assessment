package com.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Admin;
import com.dao.AdminLogin;

@Service
public class AdminLoginService {
	
	@Autowired
	AdminLogin adminLogin;
	
	public void save(Admin admin) {
		adminLogin.save(admin);
	}	
	public Admin adminLogin(String username, String password) {
		Admin admin = adminLogin.findByUsernameAndPassword(username, password);
	  	return admin;
	}

}
