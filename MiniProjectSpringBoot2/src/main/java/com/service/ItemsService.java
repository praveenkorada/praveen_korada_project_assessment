package com.service;

import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;

import com.bean.Items;
import com.dao.ItemDao;

@Service
public class ItemsService {

	@Autowired
	ItemDao itemDao;
	
	public List<Items> viewitems() {
		List<Items>items = itemDao.findAll();
		return items;
	}
	public void addItem(Items items) {
		itemDao.save(items);	
	}
	public void deleteItem(Items item) {
		
		Optional <Items> list =itemDao.findById(item.getId());
		
		if(list.isPresent()) {
			itemDao.delete(list.get());
		}
		else {
			throw new RuntimeException("items not found");
		}
	}
	
}
