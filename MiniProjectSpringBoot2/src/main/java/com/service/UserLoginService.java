package com.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.UserLogin;
import com.bean.Users;

@Service
public class UserLoginService {

	@Autowired
	UserLogin userLogin;
	
	public void save(Users users) {
		userLogin.save(users);
	}
	public Users login(String username, String password) {
		Users users = userLogin.findByUsernameAndPassword(username, password);
		return users;
	}
	public List<Users> showUser() {
		List<Users> users = userLogin.findAll();
		return users;
	}
}
