package com.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="admin")
public class Admin {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String username;
	private String password;
	
	@Transient
	private String systemCode;

	public Admin(String username, String password, String systemCode) {
		super();
		this.username = username;
		this.password = password;
		this.systemCode = systemCode;
	}

	public Admin() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSystemCode() {
		return systemCode;
	}

	public void setSystemCode(String systemCode) {
		this.systemCode = systemCode;
	}

	@Override
	public String toString() {
		return "Admin [username=" + username + ", password=" + password + ", systemCode=" + systemCode + "]";
	}
	
	
	
}
