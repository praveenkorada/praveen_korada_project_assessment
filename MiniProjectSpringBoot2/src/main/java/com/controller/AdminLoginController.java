package com.controller;

import java.util.List;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
//import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


import com.service.AdminLoginService;
import com.service.UserLoginService;
import com.bean.Admin;
import com.bean.Items;
import com.bean.Users;
import com.dao.ItemDao;



@Controller
@RequestMapping(value = "/adminl")
public class AdminLoginController {

	@Autowired
	private AdminLoginService adminLoginService;

	@Autowired
	ItemDao itemDao;

	@Autowired
	private UserLoginService userLoginService;

	@GetMapping("/adminLogin")
	public ModelAndView login() {
		ModelAndView mav = new ModelAndView("adminLogin");
		mav.addObject("admin", new Admin());
		return mav;
	}

	@PostMapping("/adminLogin")
	public ModelAndView loginProcess(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("admin") Admin admin) {

		ModelAndView mav = null;
		Admin oauthUser = adminLoginService.adminLogin(admin.getUsername(), admin.getPassword());

		System.out.print(oauthUser);
		if (Objects.nonNull(oauthUser)) {
			mav = new ModelAndView("adminPage");
			List<Items> list = itemDao.findAll();
			mav.addObject("list", list);
			mav.addObject("name", admin.getUsername());

		} else {
			mav = new ModelAndView("adminLogin");
			mav.addObject("message", "Invalid Login Credentials");
		}
		return mav;
	}

	@RequestMapping(value = "/showUsers", method = RequestMethod.GET)
	public ModelAndView showUsers(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("admin") Admin admin) {

		ModelAndView mav = null;
		mav = new ModelAndView("showUsers");
		List<Users> list = userLoginService.showUser();
		mav.addObject("list", list);
		mav.addObject("username", admin.getUsername());

		return mav;
	}
}