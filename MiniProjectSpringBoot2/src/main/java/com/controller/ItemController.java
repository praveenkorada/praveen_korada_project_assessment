package com.controller;


import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.bean.Admin;
import com.bean.Items;
import com.bean.Users;

import com.service.ItemsService;

@Controller
@RequestMapping(value = "/item")
public class ItemController {


	@Autowired
	ItemsService itemService;


	@GetMapping("/addItem")
	public ModelAndView addBooks(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("admin") Admin admin) {
		
		ModelAndView mav = new ModelAndView("addItem");
		mav.addObject("name", admin.getUsername());
		mav.addObject("Item", new Items());
		return mav;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addMenu(@ModelAttribute("menu") Items items, Model m) {

		itemService.addItem(items);
		String itemName = items.getName();
		m.addAttribute("itemName", itemName);

		List<Items> list = itemService.viewitems();
		m.addAttribute("list", list);

		return "addMenu";
	}
	@RequestMapping(value = "/deleteFromMenu", method = RequestMethod.GET)
	public ModelAndView deleteMenu(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("Items") Items items) {

		System.out.println("calling delete method...");
		ModelAndView mav = null;
		itemService.deleteItem(items);
		mav = new ModelAndView("adminPage");
		List<Items> list = itemService.viewitems();
		mav.addObject("list", list);
		mav.addObject("itemName", items.getId());
		return mav;
	}

	@RequestMapping(value = "/adminPage", method = RequestMethod.GET)
	public ModelAndView showAdminPage(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("admin") Admin admin) {

		ModelAndView mav = null;
		mav = new ModelAndView("adminPage");
		List<Items> list = itemService.viewitems();
		mav.addObject("list", list);
		mav.addObject("name", admin.getUsername());
		return mav;
	}

	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	public ModelAndView showWelcomePage(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("users") Users users) {

		ModelAndView mav = null;
		mav = new ModelAndView("welcome");
		List<Items> list = itemService.viewitems();
		mav.addObject("list", list);
		mav.addObject("name", users.getUsername());
		return mav;
	}
}
