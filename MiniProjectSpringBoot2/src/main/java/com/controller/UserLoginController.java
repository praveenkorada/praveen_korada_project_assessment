package com.controller;

import java.util.List;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.bean.Items;
import com.bean.Users;
import com.dao.ItemDao;
import com.service.UserLoginService;

@Controller
@RequestMapping(value = "/user")
public class UserLoginController {

	@Autowired
	private UserLoginService userLoginService;

	@Autowired
	 ItemDao itemDao;

	@GetMapping("/login")
	public ModelAndView login() {
		ModelAndView mav = new ModelAndView("login");
		mav.addObject("user", new Users());
		return mav;
	}

	@PostMapping("/login")
	public ModelAndView loginProcess(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("users") Users users) {

		ModelAndView mav = null;

		Users oauthUser = userLoginService.login(users.getUsername(), users.getPassword());

		System.out.print(oauthUser);
		if (Objects.nonNull(oauthUser)) {

			mav = new ModelAndView("welcome");
			List<Items> list = itemDao.findAll();
			mav.addObject("list", list);
			mav.addObject("name", users.getUsername());
			mav.addObject("id", users.getUserid());
		} else {
			mav = new ModelAndView("login");
			mav.addObject("message", "Username or Password is wrong!");
		}
		return mav;
	}

}

