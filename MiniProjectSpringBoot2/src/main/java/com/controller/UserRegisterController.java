package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bean.Users;
import com.service.UserLoginService;


@Controller
@RequestMapping(value = "/user")
public class UserRegisterController {

	@Autowired
	UserLoginService service;

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(@ModelAttribute("users") Users users) {
		service.save(users);
		return "login";
	}

}
