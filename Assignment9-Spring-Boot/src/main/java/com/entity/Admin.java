package com.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "adminregister")
public class Admin {

	@Id
	@Column(name="adminid")
	private int AdminId;
	@Column(name="username")
	private String username;
	private String password;
	public Admin() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Admin(int adminId, String username, String password) {
		super();
		this.AdminId = adminId;
		this.username = username;
		this.password = password;
	}
	public int getAdminId() {
		return AdminId;
	}
	public void setAdminId(int adminId) {
		AdminId = adminId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "AdminRegister [AdminId=" + AdminId + ", username=" + username + ", password=" + password + "]";
	}
	
	
}
