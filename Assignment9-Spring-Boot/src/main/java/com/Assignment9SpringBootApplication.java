package com;

import javax.tools.DocumentationTool.DocumentationTask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
@SpringBootApplication
@EntityScan
@EnableJpaRepositories
@EnableSwagger2
public class Assignment9SpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(Assignment9SpringBootApplication.class, args);
		System.out.println("Server is running on 8080");
	}
	@Bean
	public Docket api() {
		System.out.println("Object Created");
		return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.basePackage("com")).build();
	}

}
