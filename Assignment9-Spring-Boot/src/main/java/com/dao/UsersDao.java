package com.dao;

	import org.springframework.data.jpa.repository.JpaRepository;
	import org.springframework.data.repository.query.Param;
	import org.springframework.stereotype.Repository;

	import com.entity.Users;
	@Repository
	public interface UsersDao extends JpaRepository<Users, Integer>{

	}
