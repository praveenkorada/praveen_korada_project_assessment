package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.entity.Book;
import com.service.BookService;

@RestController
@RequestMapping("/book")
public class BookController {

	@Autowired
	BookService bookService;
	
	@GetMapping(value="getAllBook",produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Book>getAllBooks(){
		return bookService.getAllBook();
	}
	@PostMapping(value="storeBook",consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeBook(@RequestBody Book b) {
		return bookService.storeBook(b);
	}
	 
	 @DeleteMapping(value="deleteBook/{bookid}")
	 public String deleteBooks(@PathVariable("bookid")int bookid) {
		 return bookService.deleteBook(bookid);
	 }
	 
	 @PatchMapping(value="updateBook")
	 public String updateProduct(@RequestBody Book b) {
		 return bookService.updateBook(b);
	 }
	
}
