package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.entity.*;
import com.service.*;

@RestController
@RequestMapping("/users")
public class UsersController {

	@Autowired
	UsersService usersService;
	@GetMapping(value="getUsers",produces = MediaType.APPLICATION_JSON_VALUE)
	 public List<Users> getAllUsers(){
		 return usersService.getAllUsers();
	 }
	 
	 @PostMapping(value="saveUser",consumes = MediaType.APPLICATION_JSON_VALUE)
	 public String storeUsers(@RequestBody Users u) {
		 return usersService.saveUser(u);
	 }
	 
	 @DeleteMapping(value="deleteUser/{userid}")
	 public String deleteUsers(@PathVariable("userid")int userid) {
		 return usersService.deleteUser(userid);
	 }
	 
	 @PatchMapping(value="updateUsers")
	 public String updateUsers(@RequestBody Users u) {
		 return usersService.updateUser(u);
	 }
}
