package com.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.Admin;
import com.dao.AdminDao;
@Service
public class AdminService {

	@Autowired
	AdminDao adminDao;
	 public String adminRegistration(Admin admin) {
		   if(adminDao.existsById(admin.getAdminId())) {
			   return "Invalid Id";
		   }else {
			   adminDao.save(admin);
			   return "Admin Registered!!";
		   }	   
	   }
	   
	   public String checkAdminDetails(Admin admin) {
		   if(adminDao.existsById(admin.getAdminId())) {
			   Admin a=adminDao.getById(admin.getAdminId());
			   if(a.getPassword().equals(admin.getPassword())) {
				   return "Admin Logged In";
			   }else {
				   return "Invalid credentials";
			   }				   
		   }else {
			   return "Invalid credentials";
		   }
		   
	   }
}
