package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.BookDao;
import com.entity.Book;

@Service
public class BookService {

	@Autowired
	BookDao bookDao;
	
	public List<Book>getAllBook(){
		return bookDao.findAll();
	}
	public String storeBook(Book b) {
		if(bookDao.existsById(b.getBookid())) 
			return "book id should be unique";
		else {
			bookDao.save(b);
			return "Book saved!!";
		}
	}
	public String deleteBook(int bookid) {
		if(!bookDao.existsById(bookid))
			return "Book id is not present";
		else {
			bookDao.deleteById(bookid);
			return "Book Deleted!!";
		}
	}
	public String updateBook(Book b) {
		if(!bookDao.existsById(b.getBookid()))
			return "Book id not present ";
		else {
			Book book=bookDao.getById(b.getBookid());
			book.setPrice(b.getPrice());
			bookDao.saveAndFlush(book);
			return "Book updated!!1";
		}
	}
}
