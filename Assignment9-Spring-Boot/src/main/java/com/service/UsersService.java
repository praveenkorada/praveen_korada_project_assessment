package com.service;

	import java.util.List;

	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.stereotype.Service;

	import com.entity.*;
	import com.entity.Users;
	import com.dao.UsersDao;

	@Service
	public class UsersService {

		@Autowired
		UsersDao usersDao;
		
		public List<Users> getAllUsers(){
	  	  return usersDao.findAll();
	    }
	    
	    public String saveUser(Users u) {
	  	  if(usersDao.existsById(u.getUserid())) {
	  		  return "Invalid User Id";
	  	  }else {
	  		  usersDao.save(u);
	  		  return "User Saved!!";
	  	  }
	    }
	    
	    public String deleteUser(int username) {
	  	  if(!usersDao.existsById(username)){
	  		  return "Invalid Username";
	  	  }else {
	  		  usersDao.deleteById(username);
	  		  return "User deleted!!";
	  	  }
	    }
	    
	    public String updateUser(Users u) {
	  	  if(!usersDao.existsById(u.getUserid())){
	  		  return "Invalid User Id";
	  	  }else {
	  		  Users users=usersDao.getById(u.getUserid());
	  		  users.setPassword(u.getPassword());
	  		  usersDao.saveAndFlush(users);
	  		  return "password changed successfully";
	  	  }
	    }
	}


