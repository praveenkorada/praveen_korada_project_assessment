create database week9_praveen;
use week9_praveen;

create table users(
username varchar(30) primary key,
password varchar(30),
userid int
);

create table adminregister(
adminid int primary key
username varchar(30),
password varchar(30),

);

create table book(bookid int primary key,name varchar(50),author varchar(100),price float,url varchar(1000));

insert into book values(1,"Piranesi","Susanna Clarke (2021)",599,"https://media.wired.co.uk/photos/617816d6b7a9cc58f009bb8a/master/w_1600,c_limit/26082021_B_02.jpg");
insert into book values(2,"Stardust","Neil Gaiman (2010)",788,"https://media.wired.co.uk/photos/617816cee82d86967f88fe5f/master/w_1600,c_limit/26082021_B_04.jpg");
insert into book values(3,"Jade City"," Fonda Lee (2018)",988,"https://media.wired.co.uk/photos/617816cce82d86967f88fe5d/master/w_640,c_limit/26082021_B_01.jpg");
insert into book values(4,"The Last Unicorn","Peter S Beagle (1968)",899,"https://media.wired.co.uk/photos/617816ce987920b1978506ec/master/w_640,c_limit/26082021_B_05.jpg");
insert into book values(5,"A Court of Thorns and Roses","Sarah J. Maas (2020)",567,"https://media.wired.co.uk/photos/606d9f230286a2e569b12c39/master/w_1600,c_limit/wired-books-5.jpg");
insert into book values(6,"The Power","Naomi Alderman (2017)",806,"https://media.wired.co.uk/photos/606d9f23938ecee6e930e6c8/master/w_1600,c_limit/wired-books-29.jpg");
insert into book values(7,"The Fifth Season","N. K. Jemisin (2016)",767,"https://media.wired.co.uk/photos/606d9f236ab54fce4fbb1ebc/master/w_960,c_limit/wired-books-26.jpg");
